V's Barbershop is a family owned barbershop located in Old City Philadelphia. Our barbers are dedicated to providing a great experience for every customer. Included with each great cut is a hot lather neck shave, hot towel treatment, and optional massage. We welcome customers to come in and relax.

Address: 58 North 2nd Street, Philadelphia, PA 19106, USA

Phone: 445-444-0351

Website: [https://vbarbershop.com/locations/old-city-philadelphia](https://vbarbershop.com/locations/old-city-philadelphia)
